package com.addonis.addonis.repositories;

import com.addonis.addonis.exceptions.EntityNotFoundException;
import com.addonis.addonis.repositories.contracts.BaseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static java.lang.String.format;


public abstract class AbstractBaseRepository<T> implements BaseRepository<T>{

    private final Class<T> clazz;
    private final SessionFactory sessionFactory;

    @Autowired
    public AbstractBaseRepository(Class<T> clazz, SessionFactory sessionFactory) {
        this.clazz = clazz;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public <V> T getByField(String name, V value) {
        final String query = format("from %s where %s = :value", clazz.getSimpleName(), name);

        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery(query, clazz)
                    .setParameter("value", value)
                    .uniqueResultOptional()
                    .orElseThrow(EntityNotFoundException::new);
        }
    }

    @Override
    public T getById(int id) {
        return getByField("id", id);
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(format("from %s ", clazz.getName()), clazz).list();
        }
    }
}
