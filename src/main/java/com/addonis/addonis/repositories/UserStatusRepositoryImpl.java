package com.addonis.addonis.repositories;

import com.addonis.addonis.models.UserStatus;
import com.addonis.addonis.repositories.contracts.UserStatusRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class UserStatusRepositoryImpl extends AbstractBaseRepository<UserStatus> implements UserStatusRepository {

    private final SessionFactory sessionFactory;

    public UserStatusRepositoryImpl(SessionFactory sessionFactory) {
        super(UserStatus.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
