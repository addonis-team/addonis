package com.addonis.addonis.repositories;

import com.addonis.addonis.models.AddonStatus;
import com.addonis.addonis.repositories.contracts.AddonStatusRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class AddonStatusRepositoryImpl extends AbstractBaseRepository<AddonStatus> implements AddonStatusRepository {

    private final SessionFactory sessionFactory;

    public AddonStatusRepositoryImpl(SessionFactory sessionFactory) {
        super(AddonStatus.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
