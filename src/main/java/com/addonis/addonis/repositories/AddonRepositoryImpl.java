package com.addonis.addonis.repositories;

import com.addonis.addonis.models.Addon;
import com.addonis.addonis.repositories.contracts.AddonRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static com.addonis.addonis.models.utils.ModelConstants.*;

@Repository
public class AddonRepositoryImpl extends AbstractCRUDRepository<Addon> implements AddonRepository {

    private static final String[] SORT_PARAMS = new String[]{"name", "downloads", "date created", "last commit date"};

    private final SessionFactory sessionFactory;

    @Autowired
    public AddonRepositoryImpl(SessionFactory sessionFactory) {
        super(Addon.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Addon> getFeatured() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery(" from Addon where status.id = :status ", Addon.class);
            query.setParameter("status", ADDON_STATUS_FEATURED_ID);
            return query.list();
        }
    }

    @Override
    public List<Addon> getMostPopular() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery(" from Addon where status.id != :pendingStatusId order by downloads desc", Addon.class);
            setPendingStatusIdValue(query);
            query.setMaxResults(MOST_POPULAR_ADDONS_DISPLAYED);
            return query.list();
        }
    }

    @Override
    public List<Addon> getMostRecent() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery(" from Addon where status.id != :pendingStatusId order by createDate desc", Addon.class);
            setPendingStatusIdValue(query);
            query.setMaxResults(MOST_RECENT_ADDONS_DISPLAYED);
            return query.list();
        }
    }

    @Override
    public List<Addon> getActiveAddons() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery(" from Addon where status.id != :pendingStatusId ", Addon.class);
            setPendingStatusIdValue(query);
            return query.list();
        }
    }

    @Override
    public List<String> getSortParams() {
        return List.of(SORT_PARAMS);
    }

    @Override
    public List<Addon> filter(Optional<Integer> creatorId, Optional<Integer> ideId, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from Addon where status.id != 2");
            List<String> filters = new ArrayList<>();
            HashMap<String, Integer> parameters = new HashMap<>();

            creatorId.ifPresent(value -> {
                filters.add(" creator_id = :creatorId ");
                parameters.put("creatorId", value);
            });
            ideId.ifPresent(value -> {
                filters.add(" IDE_id = :ideId ");
                parameters.put("ideId", value);
            });

            if (!filters.isEmpty()) {
                queryString.append(" and ").append(String.join(" and ", filters));
            }

            sort.ifPresent(string -> queryString.append(generateSortString(string)));

            Query<Addon> query = session.createQuery(String.valueOf(queryString), Addon.class);
            query.setProperties(parameters);

            return query.list();
        }
    }

    @Override
    public List<Addon> search(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery(" from Addon where status.id != 2 and (name like :name or ide.name like :name or creator.username like :name)", Addon.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public int getNumberOfPendingAddons() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery(
                    "select count(*) from Addon where status.id = :id", Long.class);
            query.setParameter("id", ADDON_STATUS_PENDING_ID);
            long count = query.uniqueResult();
            return (int) count;
        }
    }

    @Override
    public List<Addon> getPending() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon where status.id = :id", Addon.class);
            query.setParameter("id", ADDON_STATUS_PENDING_ID);
            return query.list();
        }
    }

    @Override
    public List<Addon> getUserAddons(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon where creator.id = :id", Addon.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    private String generateSortString(String sort) {
        StringBuilder sortString = new StringBuilder(" order by ");
        String[] paramArray = sort.split("-");
        for (int i = 0; i < paramArray.length; i++) {
            appendSortParam(sortString, paramArray[i]);
            if (i != paramArray.length - 1 && (!paramArray[i + 1].equals("desc") && !paramArray[i + 1].equals("asc"))) {
                sortString.append(", ");
            }
        }
        return sortString.toString();
    }

    private void appendSortParam(StringBuilder sortString, String param) {
        switch (param) {
            case "name":
                sortString.append(" name ");
                break;
            case "downloads":
                sortString.append(" downloads ");
                break;
            case "date created":
                sortString.append(" create_date ");
                break;
            case "last commit date":
                sortString.append(" last_commit_date ");
                break;
            case "desc":
                sortString.append(" desc ");
                break;
            case "asc":
                sortString.append(" asc");
        }
    }

    private void setPendingStatusIdValue(Query<Addon> query) {
        query.setParameter("pendingStatusId", ADDON_STATUS_PENDING_ID);
    }
}

