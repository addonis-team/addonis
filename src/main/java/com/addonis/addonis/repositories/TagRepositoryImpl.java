package com.addonis.addonis.repositories;

import com.addonis.addonis.models.Tag;
import com.addonis.addonis.repositories.contracts.TagRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class TagRepositoryImpl extends AbstractBaseRepository<Tag> implements TagRepository {

    private final SessionFactory sessionFactory;

    public TagRepositoryImpl(SessionFactory sessionFactory) {
        super(Tag.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
