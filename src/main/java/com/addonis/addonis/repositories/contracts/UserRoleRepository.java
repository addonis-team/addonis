package com.addonis.addonis.repositories.contracts;

import com.addonis.addonis.models.UserRole;

public interface UserRoleRepository extends BaseRepository<UserRole>{
}
