package com.addonis.addonis.repositories.contracts;

import com.addonis.addonis.models.Tag;

public interface TagRepository extends BaseRepository<Tag>{
}
