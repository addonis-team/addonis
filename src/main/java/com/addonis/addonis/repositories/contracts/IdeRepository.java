package com.addonis.addonis.repositories.contracts;

import com.addonis.addonis.models.IDE;

public interface IdeRepository extends BaseRepository<IDE>{
}
