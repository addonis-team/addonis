package com.addonis.addonis.repositories.contracts;

import com.addonis.addonis.models.UserStatus;

public interface UserStatusRepository extends BaseRepository<UserStatus>{
}
