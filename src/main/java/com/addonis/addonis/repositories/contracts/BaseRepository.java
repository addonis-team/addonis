package com.addonis.addonis.repositories.contracts;

import java.util.List;

public interface BaseRepository<T> {

    List<T> getAll();

    T getById(int id);

    <V> T getByField(String name, V value);
}
