package com.addonis.addonis.repositories.contracts;

import com.addonis.addonis.models.Addon;

import java.util.List;
import java.util.Optional;

public interface AddonRepository extends CRUDRepository<Addon> {

    List<Addon> getFeatured();

    List<Addon> getMostPopular();

    List<Addon> getMostRecent();

    List<Addon> getActiveAddons();

    List<String> getSortParams();

    List<Addon> filter(Optional<Integer> creatorId, Optional<Integer> ideId, Optional<String> sort);

    List<Addon> search(String name);

    int getNumberOfPendingAddons();

    List<Addon> getPending();

    List<Addon> getUserAddons(int id);
}
