package com.addonis.addonis.repositories.contracts;

import com.addonis.addonis.models.AddonStatus;

public interface AddonStatusRepository extends BaseRepository<AddonStatus>{
}
