package com.addonis.addonis.repositories.contracts;

public interface CRUDRepository<T> extends BaseRepository<T> {

    void create(T Entity);

    void update(T Entity);

    void delete(int id);
}
