package com.addonis.addonis.repositories.contracts;

import com.addonis.addonis.models.Addon_rater;

public interface AddonRaterRepository extends CRUDRepository<Addon_rater>{
    Addon_rater getAddonRaters(int addon_id, int user_id);
}
