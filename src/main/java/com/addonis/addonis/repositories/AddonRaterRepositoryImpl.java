package com.addonis.addonis.repositories;

import com.addonis.addonis.models.Addon_rater;
import com.addonis.addonis.repositories.contracts.AddonRaterRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AddonRaterRepositoryImpl extends AbstractCRUDRepository<Addon_rater> implements AddonRaterRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public AddonRaterRepositoryImpl(SessionFactory sessionFactory) {
        super(Addon_rater.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Addon_rater getAddonRaters(int addon_id, int user_id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon_rater> query = session.createQuery("from Addon_rater where addon_id = :addon and user_id = :user", Addon_rater.class);
            query.setParameter("addon", addon_id);
            query.setParameter("user", user_id);
            return query.list().get(0);
        }
    }
}
