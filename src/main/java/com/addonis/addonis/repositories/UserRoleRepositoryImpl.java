package com.addonis.addonis.repositories;

import com.addonis.addonis.models.UserRole;
import com.addonis.addonis.repositories.contracts.UserRoleRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class UserRoleRepositoryImpl extends AbstractBaseRepository<UserRole> implements UserRoleRepository {

    private final SessionFactory sessionFactory;

    public UserRoleRepositoryImpl(SessionFactory sessionFactory) {
        super(UserRole.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
