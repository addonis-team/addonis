package com.addonis.addonis.repositories;

import com.addonis.addonis.models.IDE;
import com.addonis.addonis.repositories.contracts.IdeRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class IdeRepositoryImpl extends AbstractBaseRepository<IDE> implements IdeRepository {

    private final SessionFactory sessionFactory;

    public IdeRepositoryImpl(SessionFactory sessionFactory) {
        super(IDE.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
