package com.addonis.addonis.repositories;

import com.addonis.addonis.exceptions.EntityNotFoundException;
import com.addonis.addonis.models.User;
import com.addonis.addonis.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;


@Repository
public class UserRepositoryImpl extends AbstractCRUDRepository<User> implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where status.id in (1,2)", User.class);
            return query.list();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where id = :id and status.id in (1,2)", User.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", id);
            }
            return query.list().get(0);
        }
    }

    @Override
    public List<User> search(Optional<String> username, Optional<String> email, Optional<String> phone) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from User where status.id in (1,2) ");
            List<String> queryConditions = new ArrayList<>();
            HashMap<String, Object> parameters = new HashMap<>();

            username.ifPresent(value -> {
                queryConditions.add("username like :name");
                parameters.put("name", "%" + value + "%");
            });

            email.ifPresent(value -> {
                queryConditions.add("email like :email");
                parameters.put("email", "%" + value + "%");
            });

            phone.ifPresent(value -> {
                queryConditions.add("phone like :phone");
                parameters.put("phone", value);
            });

            if (!queryConditions.isEmpty()) {
                queryString.append(" and ")
                        .append(String.join(" and ", queryConditions));
            }

            Query<User> query = session.createQuery(queryString.toString(), User.class);
            query.setProperties(parameters);
            return query.list();
        }
    }

    @Override
    public List<User> searchAllFields(String word) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where status.id in (1,2) and (username like :word or email like :word or phone like :word)", User.class);
            query.setParameter("word", "%" + word + "%");
            return query.list();
        }
    }


    @Override
    public int getUserAddonsNumber(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery(
                    "select count(*) from Addon where creator.id = :id", Long.class);
            query.setParameter("id", id);
            long count = query.uniqueResult();
            return (int) count;
        }
    }
}
