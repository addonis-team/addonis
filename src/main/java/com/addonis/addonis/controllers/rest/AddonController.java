package com.addonis.addonis.controllers.rest;

import com.addonis.addonis.controllers.AuthenticationHelper;
import com.addonis.addonis.exceptions.*;
import com.addonis.addonis.models.Addon;
import com.addonis.addonis.models.Dto.AddonDto;
import com.addonis.addonis.models.User;
import com.addonis.addonis.services.contracts.AddonService;
import com.addonis.addonis.services.utils.AddonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api/addons")
@EnableScheduling
public class AddonController {

    private final AddonService addonService;
    private final AuthenticationHelper authenticationHelper;
    private final AddonMapper addonMapper;

    @Autowired
    public AddonController(AddonService addonService,
                           AuthenticationHelper authenticationHelper,
                           AddonMapper addonMapper) {
        this.addonService = addonService;
        this.authenticationHelper = authenticationHelper;
        this.addonMapper = addonMapper;
    }

    @GetMapping
    public List<Addon> getAll() {
        return addonService.getAll();
    }

    @GetMapping("/{id}")
    public Addon getById(@PathVariable int id) {
        try {
            return addonService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/download")
    public ResponseEntity<Resource> download(@PathVariable int id) {
        Resource file = addonService.download(id);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping("/filter")
    public List<Addon> filter(@RequestParam(required = false) Optional<Integer> creatorId,
                              @RequestParam(required = false) Optional<Integer> ideId,
                              @RequestParam(required = false) Optional<String> sort) {
        return addonService.filter(creatorId, ideId, sort);
    }

    @GetMapping("/search")
    public List<Addon> search(@RequestParam String name) {
        return addonService.search(name);
    }

    @GetMapping("/featured")
    public List<Addon> getFeatured() {
        return addonService.getFeatured();
    }

    @GetMapping("/most-popular")
    public List<Addon> getMostPopular() {
        return addonService.getMostPopular();
    }

    @GetMapping("/most-recent")
    public List<Addon> getMostRecent() {
        return addonService.getMostRecent();
    }

    @PostMapping
    public Addon create(@Valid @ModelAttribute AddonDto dto,
                        @RequestHeader HttpHeaders headers) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        try {
            Addon addon = addonMapper.addonFromDto(dto, loggedUser);
            MultipartFile file = dto.getFile();
            return addonService.create(loggedUser, addon, file);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UserBlockedException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (StorageServiceException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Addon update(@PathVariable int id, @Valid @RequestBody AddonDto dto, @RequestHeader HttpHeaders headers) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        try {
            Addon addon = addonMapper.addonFromDto(id, dto, loggedUser);
            addonService.update(loggedUser, addon);
            return addon;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UserBlockedException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @PutMapping("/{id}/approve")
    public Addon approve(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        try {
            return addonService.approve(loggedUser, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{id}/rate")
    public Addon rate(@RequestHeader HttpHeaders headers,
                      @PathVariable int id,
                      @RequestParam int rating) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        try {
            return addonService.rate(id, rating, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        try {
            addonService.delete(loggedUser, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ExceptionHandler(StorageServiceException.class)
    public void handleStorageServiceException(StorageServiceException exception) {
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler(UnauthorizedOperationException.class)
    public void handleUnauthorizedOperationException(UnauthorizedOperationException exception) {
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, exception.getMessage());
    }

}
