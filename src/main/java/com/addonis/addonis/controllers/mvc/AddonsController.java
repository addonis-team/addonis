package com.addonis.addonis.controllers.mvc;

import com.addonis.addonis.controllers.AuthenticationHelper;
import com.addonis.addonis.exceptions.*;
import com.addonis.addonis.models.Addon;
import com.addonis.addonis.models.Dto.AddonDto;
import com.addonis.addonis.models.IDE;
import com.addonis.addonis.models.Tag;
import com.addonis.addonis.models.User;
import com.addonis.addonis.services.contracts.AddonService;
import com.addonis.addonis.services.contracts.IdeService;
import com.addonis.addonis.services.utils.AddonMapper;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/addons")
public class AddonsController {
    private final AddonService addonService;
    private final IdeService ideService;
    private final AuthenticationHelper authenticationHelper;
    private final AddonMapper addonMapper;

    public AddonsController(AddonService addonService, IdeService ideService, AuthenticationHelper authenticationHelper,
                            AddonMapper addonMapper) {
        this.addonService = addonService;
        this.ideService = ideService;
        this.authenticationHelper = authenticationHelper;
        this.addonMapper = addonMapper;
    }


    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session, Model model) {
        if (session.getAttribute("currentUser") != null) {
            model.addAttribute("loggedUser", authenticationHelper.tryGetUser(session));
            return true;
        }
        return false;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            return authenticationHelper.tryGetUser(session).isAdmin();
        }
        return false;
    }

    @ModelAttribute("sortParamsAvailable")
    public List<String> populateSortParams() {
        return addonService.getSortParams();
    }

    @ModelAttribute("pending")
    public int numberOfPendingAddons() {
        return addonService.getNumberOfPendingAddons();
    }

    @ModelAttribute("ides")
    public List<IDE> listIDEs() {
        return ideService.getAll();
    }

    @ModelAttribute("tags")
    public List<Tag> populateTags() {
        return addonService.getAllTags();
    }

    @GetMapping
    public String listAllAddons(Model model) {
        List<Addon> addons = addonService.getActiveAddons();
        model.addAttribute("addons", addons);
        return "addons";
    }

    @GetMapping("/{id}")
    public String showAddonDetailsPage(@PathVariable int id, Model model) {
        Addon addon = addonService.getById(id);
        model.addAttribute("addon", addon);
        return "addon-details";
    }

    @GetMapping("/new")
    public String showCreateAddonPage(HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        if (user.isBlocked()) {
            return "access-denied";
        }

        model.addAttribute("addon", new AddonDto());
        return "addons-new";
    }

    @PostMapping("/new")
    public String createAddon(@Valid @ModelAttribute("addon") AddonDto addonDto,
                              BindingResult bindingResult,
                              HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        if (user.isBlocked()) {
            return "access-denied";
        }

        MultipartFile file = addonDto.getFile();
        if (bindingResult.hasErrors()) {
            return "addons-new";
        }

        if (file.isEmpty()) {
            bindingResult.rejectValue("file", "file_error", "File cannot be empty");
            return "addons-new";
        }

        if (addonDto.getIdeId() == 0) {
            bindingResult.rejectValue("ideId", "ide_error", "Please select a target IDE");
            return "addons-new";
        }

        try {
            Addon addon = addonMapper.addonFromDto(addonDto, user);
            addonService.create(user, addon, file);
            return "redirect:/";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "name_error", e.getMessage());
            return "addons-new";
        } catch (UserBlockedException e) {
            bindingResult.addError(new ObjectError("username", e.getMessage()));
            return "addons-new";
        }
    }

    @GetMapping("/update/{id}")
    public String showUpdateAddonPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        if (user.isBlocked()) {
            return "access-denied";
        }

        Addon addon = addonService.getById(id);
        if (!user.isAdmin() && !user.equals(addon.getCreator())) {
            return "access-denied";
        }
        AddonDto dto = addonMapper.toDto(addon);
        model.addAttribute("addonDto", dto);
        return "addons-update";
    }

    @PostMapping("/update/{id}")
    public String handleUpdateAddon(@PathVariable int id,
                                    @Valid @ModelAttribute("addonDto") AddonDto dto,
                                    BindingResult bindingResult,
                                    HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        if (bindingResult.hasErrors()) {
            return "addons-update";
        }

        try {
            Addon addon = addonMapper.addonFromDto(id, dto, loggedUser);
            MultipartFile file = dto.getFile();
            addonService.update(loggedUser, addon, file);
            return "redirect:/addons/" + id;
        } catch (EntityNotFoundException e) {
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }

    }

    @GetMapping("/download/{id}")
    public ResponseEntity<?> download(@PathVariable int id) {
        try {
            Resource file = addonService.download(id);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                    "attachment; filename=\"" + file.getFilename() + "\"").body(file);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/filter-by-ide")
    public String filterAddonsByIde(@RequestParam(name = "filter-ide", required = false) Optional<Integer> ide, Model model) {
        List<Addon> filterAddons = addonService.filter(Optional.empty(), ide, Optional.empty());
        model.addAttribute("addons", filterAddons);
        return "addons";
    }

    @GetMapping("/sort")
    public String sortAddonsBy(@RequestParam(name = "sortParam") String sortParam, Model model) {
        if (!sortParam.isBlank()) {
            List<Addon> sortAddons = addonService.filter(Optional.empty(), Optional.empty(), Optional.of(sortParam));
            model.addAttribute("addons", sortAddons);
            return "addons";
        }
        return "redirect:/addons";
    }

    @GetMapping("/pending")
    public String listPendingAddons(HttpSession session, Model model) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("loggedUser", loggedUser);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        try {
            List<Addon> addons = addonService.getPending(loggedUser);
            model.addAttribute("addons", addons);
            return "pending-addons";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/approve/{id}")
    public String approveAddon(@PathVariable int id, HttpSession session, Model model) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("loggedUser", loggedUser);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            addonService.approve(loggedUser, id);
            return "redirect:/addons/pending";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        } catch (EntityNotFoundException e) {
            return "not-found";
        }
    }

    @GetMapping("/my-addons")
    public String listMyAddons(HttpSession session, Model model) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("loggedUser", loggedUser);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        try {
            List<Addon> addons = addonService.getUserAddons(loggedUser);
            model.addAttribute("addons", addons);
            return "my-addons";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/delete/{id}")
    public String deleteAddon(@PathVariable int id, HttpSession session, Model model,
                              RedirectAttributes attributes) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("loggedUser", loggedUser);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            addonService.delete(loggedUser, id);
            return "redirect:/addons/my-addons";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        } catch (EntityNotFoundException e) {
            return "not-found";
        } catch (StorageServiceException e) {
            attributes.addFlashAttribute("errorMessage", "There is a problem deleting the addon.");
            return "redirect:/addons/my-addons";
        }
    }

    @GetMapping("/rate/{id}")
    public String rateAddon(@PathVariable int id, @RequestParam(name = "rating") int rating,
                            HttpSession session, Model model) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("loggedUser", loggedUser);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            addonService.rate(id, rating, loggedUser);
            return "redirect:/addons/" + id;
        } catch (EntityNotFoundException e) {
            return "not-found";
        }
    }

    @GetMapping("/add-to-featured/{id}")
    public String addToFeaturedList(@PathVariable int id, HttpSession session, Model model) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("loggedUser", loggedUser);
        } catch (
                AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            addonService.addToFeaturedList(loggedUser, id);
            return "redirect:/";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        } catch (EntityNotFoundException e) {
            return "not-found";
        }
    }

    @GetMapping("/remove-from-featured/{id}")
    public String removeFromFeaturedList(@PathVariable int id, HttpSession session, Model model) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("loggedUser", loggedUser);
        } catch (
                AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            addonService.removeFromFeaturedList(loggedUser, id);
            return "redirect:/";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        } catch (EntityNotFoundException e) {
            return "not-found";
        }
    }
}
