package com.addonis.addonis.controllers.mvc;

import com.addonis.addonis.controllers.AuthenticationHelper;
import com.addonis.addonis.exceptions.*;
import com.addonis.addonis.models.Dto.UpdateUserDto;
import com.addonis.addonis.models.User;
import com.addonis.addonis.services.contracts.AddonService;
import com.addonis.addonis.services.contracts.UserService;
import com.addonis.addonis.services.utils.UserMapper;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/user")
public class UserProfileController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AddonService addonService;
    private final AuthenticationHelper authenticationHelper;

    public UserProfileController(UserService userService, UserMapper userMapper, AddonService addonService, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.addonService = addonService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session, Model model) {
        if (session.getAttribute("currentUser") != null) {
            model.addAttribute("loggedUser", authenticationHelper.tryGetUser(session));
            return true;
        }
        return false;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            return authenticationHelper.tryGetUser(session).isAdmin();
        }
        return false;
    }

    @ModelAttribute("pending")
    public int numberOfPendingAddons() {
        return addonService.getNumberOfPendingAddons();
    }

    @GetMapping("/{id}")
    public String showUserProfile(@PathVariable int id, HttpSession session, Model model) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("loggedUser", loggedUser);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            User user = userService.getById(loggedUser, id);
            model.addAttribute("user", user);
            model.addAttribute("userDto", new UpdateUserDto());
            int addonsNumber = userService.getUserAddonsNumber(user, id);
            model.addAttribute("addonsNumber", addonsNumber);
            return "user-profile";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        } catch (EntityNotFoundException e) {
            return "not-found";
        }
    }

    @PostMapping("/{id}")
    public String updateUserProfile(@PathVariable int id,
                                    @Valid @ModelAttribute("userDto") UpdateUserDto dto,
                                    BindingResult bindingResult, HttpSession session,
                                    Model model, RedirectAttributes attributes) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            User userToUpdate = userService.getById(user, id);
            int addonsNumber = userService.getUserAddonsNumber(user, id);
            model.addAttribute("addonsNumber", addonsNumber);

            if (bindingResult.hasErrors()) {
                model.addAttribute("user", userToUpdate);
                return "user-profile";
            }

            try {
                if (!dto.getNewPassword().isBlank()) {
                    if (dto.getPassword().isBlank() || !dto.getPassword().equals(userToUpdate.getPassword())) {
                        bindingResult.rejectValue("password", "password-error",
                                "Invalid password");
                        model.addAttribute("user", userToUpdate);
                        return "user-profile";
                    }
                    if (!dto.getNewPassword().equals(dto.getConfirmPassword())) {
                        bindingResult.rejectValue("confirmPassword", "password-error",
                                "The two passwords do not match.");
                        model.addAttribute("user", userToUpdate);
                        return "user-profile";
                    }
                }
                User updatedUser = userMapper.updateUserFromDto(id, dto);
                userService.update(user, updatedUser);
                attributes.addFlashAttribute("successMessage", "Update successfully!");
                return "redirect:/user/" + userToUpdate.getId();
            } catch (UnauthorizedOperationException e) {
                return "access-denied";
            } catch (DuplicateEntityException e) {
                bindingResult.rejectValue("email", "email-error", e.getMessage());
                model.addAttribute("user", userToUpdate);
                return "user-profile";
            }
        } catch (EntityNotFoundException e) {
            return "not-found";
        }
    }

    @GetMapping("/{id}/picture")
    @ResponseBody
    public ResponseEntity<Resource> loadProfilePicture(@PathVariable int id, HttpSession session) {
            User loggedUser = authenticationHelper.tryGetUser(session);
            Resource resource = userService.getImageAsResource(loggedUser, id);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "inline").body(resource);
    }

    @PostMapping("/upload/{id}")
    public String uploadPhoto(@PathVariable int id,
                              @RequestParam("file") MultipartFile file,
                              HttpSession session, RedirectAttributes attributes) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        if (file.isEmpty()) {
            attributes.addFlashAttribute("message", "Please select a file to upload.");
            return "redirect:/user/" + id;
        }
        try {
            userService.uploadPhoto(loggedUser, id, file);
            attributes.addFlashAttribute("message", "Update successfully!");
            return "redirect:/user/" + id;
        } catch (EntityNotFoundException | StorageServiceException e) {
            attributes.addFlashAttribute("message", e.getMessage());
            return "redirect:/user/" + id;
        }
    }

    @GetMapping("/remove-photo/{id}")
    public String removePhoto(@PathVariable int id,
                              HttpSession session, RedirectAttributes attributes) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            userService.removePhoto(loggedUser, id);
            attributes.addFlashAttribute("message", "Update successfully!");
            return "redirect:/user/" + id;
        } catch (EntityNotFoundException | StorageServiceException e) {
            attributes.addFlashAttribute("message", e.getMessage());
            return "redirect:/user/" + id;
        }
    }
}
