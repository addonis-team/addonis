package com.addonis.addonis.controllers.mvc;

import com.addonis.addonis.controllers.AuthenticationHelper;
import com.addonis.addonis.exceptions.AuthenticationFailureException;
import com.addonis.addonis.exceptions.UnauthorizedOperationException;
import com.addonis.addonis.models.Addon;
import com.addonis.addonis.models.IDE;
import com.addonis.addonis.models.User;
import com.addonis.addonis.services.contracts.AddonService;
import com.addonis.addonis.services.contracts.IdeService;
import com.addonis.addonis.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomePageController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final AddonService addonService;
    private final IdeService ideService;

    public HomePageController(UserService userService, AuthenticationHelper authenticationHelper, AddonService addonService, IdeService ideService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.addonService = addonService;
        this.ideService = ideService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session, Model model) {
        if (session.getAttribute("currentUser") != null) {
            model.addAttribute("loggedUser", authenticationHelper.tryGetUser(session));
            return true;
        }
        return false;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            return authenticationHelper.tryGetUser(session).isAdmin();
        }
        return false;
    }

    @ModelAttribute("pending")
    public int numberOfPendingAddons() {
        return addonService.getNumberOfPendingAddons();
    }

    @ModelAttribute("ides")
    public List<IDE> listIDEs() {
        return ideService.getAll();
    }

    @ModelAttribute("featuredList")
    public List<Addon> populateFeaturedList() {
        return addonService.getFeatured();
    }

    @ModelAttribute("mostPopularList")
    public List<Addon> populateMostPopularList() {
        return addonService.getMostPopular();
    }

    @ModelAttribute("mostRecentList")
    public List<Addon> populateMostRecentList() {
        return addonService.getMostRecent();
    }

    @GetMapping
    public String showHomePage() {
        return "index";
    }

    @GetMapping("/search")
    public String search(@RequestParam(name = "searchBy") String searchBy,
                         @RequestParam(name = "search") String searchParam,
                         HttpSession session, Model model) {
        if (searchBy.equalsIgnoreCase("users")) {
            try {
                User loggedUser = authenticationHelper.tryGetUser(session);
                if (searchParam.isBlank()) {
                    return "redirect:/users";
                }
                List<User> filterUsers = userService.searchAllFields(loggedUser, searchParam);
                model.addAttribute("users", filterUsers);
                return "users";
            } catch (AuthenticationFailureException e) {
                return "redirect:/login";
            } catch (UnauthorizedOperationException e) {
                return "access-denied";
            }
        } else {
            if (searchParam.isBlank()) {
                return "redirect:/addons";
            }
            List<Addon> filterAddons = addonService.search(searchParam);
            model.addAttribute("addons", filterAddons);
            model.addAttribute("sortParamsAvailable", addonService.getSortParams());
            return "addons";
        }
    }
}
