package com.addonis.addonis.controllers.mvc;

import com.addonis.addonis.controllers.AuthenticationHelper;
import com.addonis.addonis.exceptions.AuthenticationFailureException;
import com.addonis.addonis.exceptions.UnauthorizedOperationException;
import com.addonis.addonis.models.User;
import com.addonis.addonis.services.contracts.AddonService;
import com.addonis.addonis.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UsersController {

    private final UserService userService;
    private final AddonService addonService;
    private final AuthenticationHelper authenticationHelper;

    public UsersController(UserService userService, AddonService addonService, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.addonService = addonService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session, Model model) {
        if (session.getAttribute("currentUser") != null) {
            model.addAttribute("loggedUser", authenticationHelper.tryGetUser(session));
            return true;
        }
        return false;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            return authenticationHelper.tryGetUser(session).isAdmin();
        }
        return false;
    }

    @ModelAttribute("pending")
    public int numberOfPendingAddons() {
        return addonService.getNumberOfPendingAddons();
    }

    @GetMapping
    public String listAllUsers(HttpSession session, Model model) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            List<User> users = userService.getAll(loggedUser);
            model.addAttribute("users", users);
            return "users";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable int id, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        try {
            userService.delete(loggedUser, id);
            return "redirect:/users";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/block/{id}")
    public String blockUser(@PathVariable int id, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        try {
            userService.block(loggedUser, id);
            return "redirect:/users";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/unblock/{id}")
    public String unblockUser(@PathVariable int id, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        try {
            userService.unblock(loggedUser, id);
            return "redirect:/users";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

}
