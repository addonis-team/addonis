package com.addonis.addonis.services;

import com.addonis.addonis.services.contracts.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@Service
public class StorageServiceImpl implements StorageService {

    public static final String STORAGE_DIR_PATH = System.getProperty("user.dir") + File.separator + "storage";
    public static final String PROFILE_PICS_PATH = STORAGE_DIR_PATH + File.separator + "profile_pics";
    public static final String ADDON_STORAGE_PATH = STORAGE_DIR_PATH + File.separator + "addons";
    public static final String DEFAULT_PROFILE_PICTURE_PATH = PROFILE_PICS_PATH + File.separator + "default.jpg";

    private final ApplicationContext applicationContext;

    @Autowired
    public StorageServiceImpl(ApplicationContext applicationContext) throws IOException {
        createStorageDirs();
        this.applicationContext = applicationContext;
    }

    @Override
    public void store(MultipartFile file, String storageDirectory) throws IOException {
        Path storagePath = Path.of(storageDirectory);
        if (Files.notExists(storagePath)) {
            Files.createDirectory(storagePath);
        }
        Path filePath = Paths.get(storageDirectory, file.getOriginalFilename());
        file.transferTo(filePath);
    }

    @Override
    public Resource loadAsResource(String filename) {
        URI url = Path.of(filename).toUri();
        String resourceLocation = url.toString();
        return applicationContext.getResource(resourceLocation);
    }

    @Override
    public void delete(String file) throws IOException {
        Path path = Path.of(file);
        Path parentPath = path.getParent();
        Files.delete(path);
        Files.delete(parentPath);
    }

    private void createStorageDirs() throws IOException {
        Path storagePath = Path.of(STORAGE_DIR_PATH);
        Path addonStoragePath = Path.of(ADDON_STORAGE_PATH);
        Path profilePicStoragePath = Path.of(PROFILE_PICS_PATH);
        if (Files.notExists(storagePath)) {
            Files.createDirectory(storagePath);
        }
        if (Files.notExists(addonStoragePath)) {
            Files.createDirectory(addonStoragePath);
        }
        if (Files.notExists(profilePicStoragePath)) {
            Files.createDirectory(profilePicStoragePath);
        }
    }
}
