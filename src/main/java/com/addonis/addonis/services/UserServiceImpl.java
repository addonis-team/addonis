package com.addonis.addonis.services;

import com.addonis.addonis.exceptions.DuplicateEntityException;
import com.addonis.addonis.exceptions.EntityNotFoundException;
import com.addonis.addonis.exceptions.StorageServiceException;
import com.addonis.addonis.models.User;
import com.addonis.addonis.repositories.contracts.UserRepository;
import com.addonis.addonis.repositories.contracts.UserStatusRepository;
import com.addonis.addonis.services.contracts.StorageService;
import com.addonis.addonis.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static com.addonis.addonis.models.utils.ModelConstants.*;
import static com.addonis.addonis.services.StorageServiceImpl.DEFAULT_PROFILE_PICTURE_PATH;
import static com.addonis.addonis.services.StorageServiceImpl.PROFILE_PICS_PATH;
import static com.addonis.addonis.services.utils.AuthorizationHelper.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserStatusRepository userStatusRepository;
    private final StorageService storageService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserStatusRepository userStatusRepository, StorageService storageService) {
        this.userRepository = userRepository;
        this.userStatusRepository = userStatusRepository;
        this.storageService = storageService;
    }

    @Override
    public List<User> getAll(User user) {
        throwIfNotAdmin(user, "list all users");
        return userRepository.getAll();
    }

    @Override
    public User getById(User user, int id) {
        throwIfNotAdminOrOwner(user, id, "view another user's profile");
        return userRepository.getById(id);
    }

    @Override
    public void create(User user) {
        throwIfDuplicate("username", user.getUsername());
        throwIfDuplicate("email", user.getEmail());
        throwIfDuplicate("phone", user.getPhone());
        userRepository.create(user);
    }

    @Override
    public void update(User user, User userToUpdate) {
        if (userToUpdate.isDeleted()) {
            throw new EntityNotFoundException("User", "username", userToUpdate.getUsername());
        }
        throwIfNotAdminOrOwner(user, userToUpdate.getId(), "update another user's profile");

        User repoUser = userRepository.getById(userToUpdate.getId());
        if (!repoUser.getEmail().equals(userToUpdate.getEmail())) {
            throwIfDuplicate("email", userToUpdate.getEmail());
        }
        if (!repoUser.getPhone().equals(userToUpdate.getPhone())) {
            throwIfDuplicate("phone", userToUpdate.getPhone());
        }
        userRepository.update(userToUpdate);
    }

    @Override
    public User getByUsername(String username) {
        User user = userRepository.getByField("username", username);
        if (user.isDeleted()) {
            throw new EntityNotFoundException("User", "username", username);
        }
        return user;
    }

    @Override
    public void delete(User user, int id) {
        throwIfNotAdmin(user, "delete user's profile");
        User userToDelete = userRepository.getById(id);
        userToDelete.setStatus(userStatusRepository.getById(USER_STATUS_DELETED_ID));
        userRepository.update(userToDelete);
    }

    @Override
    public User block(User user, int id) {
        throwIfNotAdmin(user, "block user's profile");
        User userToBlock = userRepository.getById(id);
        userToBlock.setStatus(userStatusRepository.getById(USER_STATUS_BLOCK_ID));
        userRepository.update(userToBlock);
        return userToBlock;
    }

    @Override
    public User unblock(User user, int id) {
        throwIfNotAdmin(user, "unblock user's profile");
        User userToUnblock = userRepository.getById(id);
        userToUnblock.setStatus(userStatusRepository.getById(USER_STATUS_ACTIVE_ID));
        userRepository.update(userToUnblock);
        return userToUnblock;
    }

    @Override
    public List<User> search(User user, Optional<String> username, Optional<String> email, Optional<String> phone) {
        throwIfNotAdmin(user, "search users");
        return userRepository.search(username, email, phone);
    }

    @Override
    public List<User> searchAllFields(User loggedUser, String searchParam) {
        throwIfNotAdmin(loggedUser, "search users");
        return userRepository.searchAllFields(searchParam);
    }

    @Override
    public void uploadPhoto(User loggedUser, int id, MultipartFile file) {
        User userToUpdate = userRepository.getById(id);

        try {
            throwIfNotAdminOrOwner(loggedUser, userToUpdate.getId(), "update another user's profile");
            if (userToUpdate.getProfilePicturePath() != null) {
                storageService.delete(PROFILE_PICS_PATH + userToUpdate.getProfilePicturePath());
            }
            String storageDir = buildStorageDirString(userToUpdate);
            userToUpdate.setProfilePicturePath(storageDir + File.separator + file.getOriginalFilename());
            storageService.store(file, PROFILE_PICS_PATH + storageDir);
            userRepository.update(userToUpdate);
        } catch (IOException e) {
            throw new StorageServiceException(e.getMessage());
        }
    }

    @Override
    public void removePhoto(User loggedUser, int id) {
        User userToUpdate = userRepository.getById(id);
        try {
            throwIfNotAdminOrOwner(loggedUser, userToUpdate.getId(), "update another user's profile");
            String photo = PROFILE_PICS_PATH + userToUpdate.getProfilePicturePath();
            storageService.delete(photo);
            userToUpdate.setProfilePicturePath(null);
            userRepository.update(userToUpdate);
        } catch (IOException e) {
            throw new StorageServiceException(e.getMessage());
        }
    }

    @Override
    public int getUserAddonsNumber(User loggedUser, int id) {
        throwIfNotAdminOrOwner(loggedUser, id, "view another user's profile");
        return userRepository.getUserAddonsNumber(id);
    }

    @Override
    public Resource getImageAsResource(User loggedUser, int id) {
        User user = getById(loggedUser, id);
        if (user.getProfilePicturePath() == null) {
            return storageService.loadAsResource(DEFAULT_PROFILE_PICTURE_PATH);
        }
        String filePath = user.getProfilePicturePath();
        String fullPath = PROFILE_PICS_PATH + filePath;
        return storageService.loadAsResource(fullPath);
    }

    private void throwIfDuplicate(String field, String value) {
        boolean duplicateExists = true;
        try {
            userRepository.getByField(field, value);
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", field, value);
        }
    }

    private String buildStorageDirString(User user) {
        return File.separator + "user_" + user.getId();
    }
}
