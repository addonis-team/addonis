package com.addonis.addonis.services.utils;

import com.addonis.addonis.models.Addon;
import com.addonis.addonis.models.AddonStatus;
import com.addonis.addonis.models.Dto.AddonDto;
import com.addonis.addonis.models.Tag;
import com.addonis.addonis.models.User;
import com.addonis.addonis.services.contracts.AddonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import static com.addonis.addonis.models.utils.ModelConstants.ADDON_STATUS_PENDING_ID;

@Component
public class AddonMapper {

    private final AddonService addonService;

    @Autowired
    public AddonMapper(AddonService addonService) {
        this.addonService = addonService;
    }

    public Addon addonFromDto(AddonDto dto, User user) {
        Addon addon = new Addon();
        convertDtoToAddon(dto, addon, user);
        return addon;
    }

    public Addon addonFromDto(int id, AddonDto dto, User user) {
        Addon addon = addonService.getById(id);
        convertDtoToAddon(dto, addon);
        return addon;
    }

    public AddonDto toDto(Addon addon) {
        AddonDto dto = new AddonDto();
        dto.setName(addon.getName());
        dto.setIdeId(addon.getIde().getId());
        dto.setDescription(addon.getDescription());
        dto.setOriginPath(addon.getOriginPath());
        dto.setTagIds(addon.getTags().stream()
                .map(Tag::getId)
                .collect(Collectors.toSet()));
        return dto;
    }

    private void convertDtoToAddon(AddonDto dto, Addon addon) {
        addon.setName(dto.getName());
        addon.setIde(addonService.getIdeById(dto.getIdeId()));
        addon.setDescription(dto.getDescription());
        addon.setOriginPath(dto.getOriginPath());
        addon.setTags(dto.getTagIds().stream()
                .map(addonService::getTagById)
                .collect(Collectors.toSet()));
    }

    private void convertDtoToAddon(AddonDto dto, Addon addon, User user) {
        addon.setName(dto.getName());
        addon.setIde(addonService.getIdeById(dto.getIdeId()));
        addon.setCreator(user);
        addon.setDescription(dto.getDescription());
        addon.setOriginPath(dto.getOriginPath());
        addon.setStatus(getPendingStatus());
        addon.setCreateDate(LocalDateTime.now());
        addon.setTags(dto.getTagIds().stream()
                .map(addonService::getTagById)
                .collect(Collectors.toSet()));
    }

    private AddonStatus getPendingStatus() {
        return addonService.populateStatus()
                .stream()
                .filter(s -> s.getId() == ADDON_STATUS_PENDING_ID)
                .collect(Collectors.toList())
                .get(0);
    }
}
