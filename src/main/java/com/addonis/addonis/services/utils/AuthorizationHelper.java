package com.addonis.addonis.services.utils;

import com.addonis.addonis.exceptions.UnauthorizedOperationException;
import com.addonis.addonis.exceptions.UserBlockedException;
import com.addonis.addonis.models.User;

public class AuthorizationHelper {

    public static void throwIfNotAdmin(User user, String requestedAction) {
        if(!user.isAdmin()) {
            throw new UnauthorizedOperationException("admin", requestedAction);
        }
    }

    public static void throwIfNotAdminOrOwner(User user, int id, String requestedAction) {
        if(user.getId() != id && !user.isAdmin()) {
            throw new UnauthorizedOperationException("admin", requestedAction);
        }
    }

    public static void throwIfBlocked(User user) {
        if(user.isBlocked()) {
            throw new UserBlockedException();
        }
    }

}
