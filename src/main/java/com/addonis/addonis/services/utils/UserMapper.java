package com.addonis.addonis.services.utils;

import com.addonis.addonis.models.Dto.UpdateUserDto;
import com.addonis.addonis.models.Dto.UserDto;
import com.addonis.addonis.models.User;
import com.addonis.addonis.repositories.contracts.UserRepository;
import com.addonis.addonis.repositories.contracts.UserRoleRepository;
import com.addonis.addonis.repositories.contracts.UserStatusRepository;
import org.springframework.stereotype.Component;

import static com.addonis.addonis.models.utils.ModelConstants.USER_ROLE_ID;
import static com.addonis.addonis.models.utils.ModelConstants.USER_STATUS_ACTIVE_ID;

@Component
public class UserMapper {

    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final UserStatusRepository userStatusRepository;

    public UserMapper(UserRepository userRepository, UserRoleRepository userRoleRepository, UserStatusRepository userStatusRepository) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
        this.userStatusRepository = userStatusRepository;
    }

    public User userFromDto(UserDto dto) {
        User user = new User();
        convertDtoToCustomer(dto, user);
        return user;
    }

    public User updateUserFromDto(UserDto dto) {
        User user = userRepository.getByField("username", dto.getUsername());
        convertDtoToCustomer(dto, user);
        return user;
    }

    public User updateUserFromDto(int id, UpdateUserDto dto) {
        User user = userRepository.getById(id);
        convertDtoToCustomer(dto, user);
        return user;
    }

    private void convertDtoToCustomer(UserDto dto, User user) {
        user.setUsername(dto.getUsername());
        user.setPassword(dto.getPassword());
        user.setEmail(dto.getEmail());
        user.setPhone(dto.getPhone());
        user.setRole(userRoleRepository.getById(USER_ROLE_ID));
        user.setStatus(userStatusRepository.getById(USER_STATUS_ACTIVE_ID));
    }

    private void convertDtoToCustomer(UpdateUserDto dto, User user) {
        if (!dto.getEmail().isBlank()){
            user.setEmail(dto.getEmail());
        }
        if (!dto.getPhone().isBlank()) {
            user.setPhone(dto.getPhone());
        }
        if (!dto.getNewPassword().isBlank()) {
            user.setPassword(dto.getNewPassword());
        }
    }
}
