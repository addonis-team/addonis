package com.addonis.addonis.services;

import com.addonis.addonis.models.IDE;
import com.addonis.addonis.repositories.contracts.IdeRepository;
import com.addonis.addonis.services.contracts.IdeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IdeServiceImpl implements IdeService {

    private final IdeRepository ideRepository;

    public IdeServiceImpl(IdeRepository ideRepository) {
        this.ideRepository = ideRepository;
    }

    @Override
    public List<IDE> getAll() {
        return ideRepository.getAll();
    }
}
