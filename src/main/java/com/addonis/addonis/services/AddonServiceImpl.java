package com.addonis.addonis.services;

import com.addonis.addonis.exceptions.DuplicateEntityException;
import com.addonis.addonis.exceptions.EntityNotFoundException;
import com.addonis.addonis.exceptions.StorageServiceException;
import com.addonis.addonis.models.*;
import com.addonis.addonis.repositories.contracts.*;
import com.addonis.addonis.services.contracts.AddonService;
import com.addonis.addonis.services.contracts.GitHubRestApiService;
import com.addonis.addonis.services.contracts.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.addonis.addonis.models.utils.ModelConstants.*;
import static com.addonis.addonis.services.StorageServiceImpl.ADDON_STORAGE_PATH;
import static com.addonis.addonis.services.utils.AuthorizationHelper.*;

@Service
public class AddonServiceImpl implements AddonService {

    private final AddonRepository addonRepository;
    private final AddonStatusRepository addonStatusRepository;
    private final IdeRepository ideRepository;
    private final TagRepository tagRepository;
    private final StorageService storageService;
    private final GitHubRestApiService gitHubRestApiService;
    private final AddonRaterRepository addonRaterRepository;

    @Autowired
    public AddonServiceImpl(AddonRepository addonRepository,
                            AddonStatusRepository addonStatusRepository,
                            IdeRepository ideRepository,
                            TagRepository tagRepository,
                            StorageService storageService,
                            GitHubRestApiService gitHubRestApiService,
                            AddonRaterRepository addonRaterRepository) {
        this.addonRepository = addonRepository;
        this.addonStatusRepository = addonStatusRepository;
        this.ideRepository = ideRepository;
        this.tagRepository = tagRepository;
        this.storageService = storageService;
        this.gitHubRestApiService = gitHubRestApiService;
        this.addonRaterRepository = addonRaterRepository;
    }

    @Override
    public List<Addon> getAll() {
        return addonRepository.getAll();
    }

    @Override
    public Addon getById(int id) {
        return addonRepository.getById(id);
    }

    @Override
    public Resource download(int id) {
        Addon addon = getById(id);
        if (addon.getFilePath() == null) {
            throw new EntityNotFoundException();
        }
        addon.setDownloads(addon.getDownloads() + 1);
        addonRepository.update(addon);
        String filePath = addon.getFilePath();
        String fullPath = ADDON_STORAGE_PATH + filePath;
        return storageService.loadAsResource(fullPath);
    }

    @Override
    public IDE getIdeById(int id) {
        return ideRepository.getById(id);
    }

    @Override
    public Tag getTagById(int id) {
        return tagRepository.getById(id);
    }

    public List<Tag> getAllTags() {
        return tagRepository.getAll();
    }

    @Override
    public List<Addon> getFeatured() {
        return addonRepository.getFeatured();
    }

    @Override
    public List<Addon> getMostPopular() {
        return addonRepository.getMostPopular();
    }

    @Override
    public List<Addon> getMostRecent() {
        return addonRepository.getMostRecent();
    }

    @Override
    public List<Addon> getActiveAddons() {
        return addonRepository.getActiveAddons();
    }

    @Override
    public Addon create(User loggedUser, Addon addon, MultipartFile file) {
        try {
            throwIfBlocked(loggedUser);
            throwIfDuplicate(addon.getName());
            addonRepository.create(addon);
            String storageDir = buildStorageDirString(addon);
            storageService.store(file, ADDON_STORAGE_PATH + storageDir);
            addon.setFilePath(storageDir + File.separator + file.getOriginalFilename());
            addonRepository.update(addon);
            return addon;
        } catch (IOException e) {
            addonRepository.delete(addon.getId());
            throw new StorageServiceException(e.getMessage());
        }
    }

    @Override
    public void update(User loggedUser, Addon addonWithUpdates) {
        Addon addonToUpdate = addonRepository.getById(addonWithUpdates.getId());
        throwIfNotAdminOrOwner(loggedUser, addonToUpdate.getCreator().getId(), "edit another user's addon");
        throwIfBlocked(loggedUser);
        if (!addonWithUpdates.getName().equalsIgnoreCase(addonToUpdate.getName())) {
            throwIfDuplicate(addonWithUpdates.getName());
        }
        addonRepository.update(addonWithUpdates);
    }

    @Override
    public void update(User loggedUser, Addon addonWithUpdates, MultipartFile file) {
        if (file.isEmpty()) {
            update(loggedUser, addonWithUpdates);
            return;
        }
        try {
            Addon addonToUpdate = getById(addonWithUpdates.getId());
            if (addonToUpdate.getFilePath() != null) {
                storageService.delete(ADDON_STORAGE_PATH + addonToUpdate.getFilePath());
            }
            String storageDir = buildStorageDirString(addonWithUpdates);
            storageService.store(file, ADDON_STORAGE_PATH + storageDir);
            addonWithUpdates.setFilePath(storageDir + File.separator + file.getOriginalFilename());
            update(loggedUser, addonWithUpdates);
        } catch (IOException e) {
            throw new StorageServiceException(e.getMessage());
        }
    }

    @Override
    public void delete(User loggedUser, int id) {
        try {
            Addon addonToDelete = addonRepository.getById(id);
            throwIfNotAdminOrOwner(loggedUser, addonToDelete.getCreator().getId(), "delete another user's addon");
            String file = ADDON_STORAGE_PATH + addonToDelete.getFilePath();
            storageService.delete(file);
            addonRepository.delete(id);
        } catch (IOException e) {
            throw new StorageServiceException(e.getMessage());
        }
    }

    @Override
    public List<String> getSortParams() {
        return addonRepository.getSortParams();
    }

    @Override
    public List<Addon> filter(Optional<Integer> creatorId, Optional<Integer> ideId, Optional<String> sort) {
        return addonRepository.filter(creatorId, ideId, sort);
    }

    @Override
    public List<Addon> search(String name) {
        return addonRepository.search(name);
    }

    @Override
    public Addon rate(int id, int rating, User loggedUser) {
        Addon addonToRate = addonRepository.getById(id);
        if (addonToRate.getRaters().contains(loggedUser)) {
            Addon_rater object = addonRaterRepository.getAddonRaters(id, loggedUser.getId());
            addonToRate.setScore(addonToRate.getScore() - object.getRating());
            object.setRating(rating);
            addonRaterRepository.update(object);
        } else {
            addonToRate.setTotalVotes(addonToRate.getTotalVotes() + 1);
            Addon_rater object = new Addon_rater(addonToRate.getId(), loggedUser.getId(), rating);
            addonRaterRepository.create(object);
        }
        addonToRate.setScore(addonToRate.getScore() + rating);
        addonRepository.update(addonToRate);
        return addonToRate;
    }

    @Override
    public Addon approve(User loggedUser, int id) {
        throwIfNotAdmin(loggedUser, "approve pending addons");
        Addon addonToApprove = addonRepository.getById(id);
        addonToApprove.setStatus(addonStatusRepository.getById(ADDON_STATUS_ACTIVE_ID));
        addonRepository.update(addonToApprove);
        return addonToApprove;
    }

    @Override
    public List<AddonStatus> populateStatus() {
        return addonStatusRepository.getAll();
    }

    @Override
    public int getNumberOfPendingAddons() {
        return addonRepository.getNumberOfPendingAddons();
    }

    @Override
    public List<Addon> getPending(User loggedUser) {
        throwIfNotAdmin(loggedUser, "list pending addons");
        return addonRepository.getPending();
    }

    @Override
    public List<Addon> getUserAddons(User loggedUser) {
        return addonRepository.getUserAddons(loggedUser.getId());
    }

    @Override
    public void addToFeaturedList(User loggedUser, int id) {
        throwIfNotAdmin(loggedUser, "add an addon to featured list");
        Addon addon = getById(id);
        addon.setStatus(populateStatus()
                .stream()
                .filter(s -> s.getId() == ADDON_STATUS_FEATURED_ID)
                .collect(Collectors.toList())
                .get(0));
        addonRepository.update(addon);
    }

    @Override
    public void removeFromFeaturedList(User loggedUser, int id) {
        throwIfNotAdmin(loggedUser, "remove an addon from featured list");
        Addon addon = getById(id);
        addon.setStatus(populateStatus()
                .stream()
                .filter(s -> s.getId() == ADDON_STATUS_ACTIVE_ID)
                .collect(Collectors.toList())
                .get(0));
        addonRepository.update(addon);
    }

    private void throwIfDuplicate(String name) {
        boolean duplicateExists = true;
        try {
            addonRepository.getByField("name", name);
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Addon", "name", name);
        }
    }

    //@Scheduled(cron = "0 * * * * *")
    @Scheduled(cron = "0 0 3 * * *")
    private void updateAddonsPerDay() {
        List<Addon> addons = addonRepository.getAll();
        addons.forEach(this::updateAddonGitHubDetails);
    }

    private void updateAddonGitHubDetails(Addon addon) {
        if (addon.getOriginPath() != null) {
            String originPath = addon.getOriginPath();

            //get information from GitHub
            HashMap<String, String> lastCommitDetails = gitHubRestApiService.getLastCommitDetails(originPath);
            int openIssuesCount = gitHubRestApiService.getOpenIssuesCount(originPath);
            int pullRequestsCount = gitHubRestApiService.getPullRequestsCount(originPath);

            //update information for addon
            addon.setLastCommitMessage(lastCommitDetails.get("commitMessage"));
            String commitDate = lastCommitDetails.get("commitDate");
            addon.setLastCommitDate(LocalDateTime.parse(commitDate.substring(0, commitDate.length() - 1)));
            addon.setOpenIssuesCount(openIssuesCount);
            addon.setPullRequestsCount(pullRequestsCount);

            //update addon
            addonRepository.update(addon);
        }
    }

    private String buildStorageDirString(Addon addon) {
        return File.separator + addon.getId() + "_" + addon.getName();
    }
}
