package com.addonis.addonis.services;

import com.addonis.addonis.services.contracts.GitHubRestApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class GitHubRestApiServiceImpl implements GitHubRestApiService {
    private static final String BASE_URL = "https://api.github.com/";
    private static final int URL_OWNER_INDEX = 3;
    private static final int URL_REPO_INDEX = 4;

    private final RestTemplate restTemplate;

    @Autowired
    public GitHubRestApiServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public HashMap<String, String> getLastCommitDetails(String originPath) {
        String[] repoDetails = getOwnerAndRepoName(originPath);
        String url = BASE_URL + "repos/" + repoDetails[0] + "/" + repoDetails[1] + "/" + "commits/master";

        LinkedHashMap lastCommit = getResponse(url);
        LinkedHashMap commit = (LinkedHashMap) lastCommit.get("commit");
        LinkedHashMap committer = (LinkedHashMap) commit.get("committer");
        HashMap<String, String> details = new HashMap<>();
        details.put("commitDate", committer.get("date").toString());
        details.put("commitMessage", commit.get("message").toString());
        //LocalDateTime date = LocalDateTime.parse(commitDate.substring(0, commitDate.length() - 1));
        return details;
    }

    @Override
    public int getPullRequestsCount(String originPath) {
        String[] repoDetails = getOwnerAndRepoName(originPath);
        String url = BASE_URL + "repos/" + repoDetails[0] + "/" + repoDetails[1];
        LinkedHashMap issues = getResponse(url);
        return Integer.parseInt(issues.get("open_issues_count").toString());
    }

    @Override
    public int getOpenIssuesCount(String originPath) {
        String[] repoDetails = getOwnerAndRepoName(originPath);
        String url = BASE_URL + "search/issues?q=repo:" + repoDetails[0] + "/" + repoDetails[1] + " is:pr";
        LinkedHashMap issues = getResponse(url);
        return Integer.parseInt(issues.get("total_count").toString());
    }

    private LinkedHashMap getResponse(String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Map> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, Map.class);
        return new LinkedHashMap(responseEntity.getBody());
    }

    private String[] getOwnerAndRepoName(String originLink) {
        // https://github.com/{owner}/{repoName}
        String[] arr = originLink.split("/");
        String owner = arr[URL_OWNER_INDEX];
        String repoName = arr[URL_REPO_INDEX];
        return new String[]{owner, repoName};
    }
}
