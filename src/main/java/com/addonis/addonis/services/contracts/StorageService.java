package com.addonis.addonis.services.contracts;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface StorageService {

    void store(MultipartFile file, String directory) throws IOException;

    Resource loadAsResource(String filename);

    void delete(String file) throws IOException;

}