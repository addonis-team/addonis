package com.addonis.addonis.services.contracts;

import com.addonis.addonis.models.*;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface AddonService extends BaseService<Addon> {

    List<Addon> getAll();

    Addon getById(int id);

    Resource download(int id);

    IDE getIdeById(int id);

    Tag getTagById(int id);

    List<Tag> getAllTags();

    List<Addon> getFeatured();

    List<Addon> getMostPopular();

    List<Addon> getMostRecent();

    List<Addon> getActiveAddons();

    Addon create(User loggedUser, Addon addon, MultipartFile file);

    void update(User loggedUser, Addon addon, MultipartFile file);

    List<String> getSortParams();

    List<Addon> filter(Optional<Integer> creatorId, Optional<Integer> ideId, Optional<String> sort);

    List<Addon> search(String search);

    Addon rate(int id, int rating, User loggedUser);

    Addon approve(User loggedUser, int id);

    List<AddonStatus> populateStatus();

    int getNumberOfPendingAddons();

    List<Addon> getPending(User loggedUser);

    List<Addon> getUserAddons(User loggedUser);

    void addToFeaturedList(User loggedUser, int id);

    void removeFromFeaturedList(User loggedUser, int id);
}
