package com.addonis.addonis.services.contracts;

import com.addonis.addonis.models.User;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface UserService extends BaseService<User> {

    List<User> getAll(User loggedUser);

    User getById(User user, int id);

    User getByUsername(String username);

    void create(User user);

    User block(User user, int id);

    User unblock(User user, int id);

    List<User> search(User user, Optional<String> username, Optional<String> email, Optional<String> phone);

    List<User> searchAllFields(User loggedUser, String searchParam);

    void uploadPhoto(User loggedUser, int id, MultipartFile file);

    void removePhoto(User loggedUser, int id);

    int getUserAddonsNumber(User loggedUser, int id);

    Resource getImageAsResource(User loggedUser, int id);
}
