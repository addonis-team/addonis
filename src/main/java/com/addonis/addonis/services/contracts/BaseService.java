package com.addonis.addonis.services.contracts;

import com.addonis.addonis.models.User;

import java.util.List;

public interface BaseService<T> {

    void update(User loggedUser, T entityToUpdate);

    void delete(User loggedUser, int id);
}
