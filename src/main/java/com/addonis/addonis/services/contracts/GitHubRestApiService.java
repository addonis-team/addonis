package com.addonis.addonis.services.contracts;

import java.util.HashMap;

public interface GitHubRestApiService {

    HashMap<String, String> getLastCommitDetails(String originPath);

    int getPullRequestsCount(String originPath);

    int getOpenIssuesCount(String originPath);
}
