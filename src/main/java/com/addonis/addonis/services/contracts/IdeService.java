package com.addonis.addonis.services.contracts;

import com.addonis.addonis.models.IDE;

import java.util.List;

public interface IdeService {
    List<IDE> getAll();
}
