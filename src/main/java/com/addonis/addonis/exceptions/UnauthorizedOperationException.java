package com.addonis.addonis.exceptions;

public class UnauthorizedOperationException extends RuntimeException {

    public UnauthorizedOperationException(String requiredUserType, String requestedAction) {
        super(String.format("Only %s have permission to %s.", requiredUserType, requestedAction));
    }
}
