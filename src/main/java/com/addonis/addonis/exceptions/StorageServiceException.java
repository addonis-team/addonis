package com.addonis.addonis.exceptions;

public class StorageServiceException extends RuntimeException {

    public StorageServiceException(String message) {
        super(message);
    }
}
