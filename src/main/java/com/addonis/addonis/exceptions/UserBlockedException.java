package com.addonis.addonis.exceptions;

public class UserBlockedException extends RuntimeException{

    public static final String USER_BLOCKED_ERROR = "Your profile is blocked.";
    public UserBlockedException() {
        super(USER_BLOCKED_ERROR);
    }
}
