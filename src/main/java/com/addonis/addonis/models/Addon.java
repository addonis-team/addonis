package com.addonis.addonis.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "addons")
public class Addon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "IDE_id")
    private IDE ide;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    @JsonIgnoreProperties(value = {"email", "phone", "role", "status", "profilePicturePath", "deleted", "blocked", "admin"})
    private User creator;

    @Column(name = "description")
    private String description;

    @JsonIgnore
    @Column(name = "file_path")
    private String filePath;

    @Column(name = "origin_path")
    private String originPath;

    @ManyToOne
    @JoinColumn(name = "status")
    private AddonStatus status;

    @JsonIgnore
    @Column(name = "total_votes")
    private int totalVotes;

    @JsonIgnore
    @Column(name = "score")
    private int score;

    @Column(name = "downloads")
    private int downloads;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "addons_tags",
            joinColumns = @JoinColumn(name = "addon_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "addon_rater",
                joinColumns = @JoinColumn(name = "addon_id"),
                inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> raters;

    @Column(name = "last_commit_message")
    private String lastCommitMessage;

    @Column(name = "last_commit_date")
    private LocalDateTime lastCommitDate;

    @Column(name = "open_issues_count")
    private int openIssuesCount;

    @Column(name = "pull_requests_count")
    private int pullRequestsCount;

    public Addon() {
    }

    public Addon(int id, String name, IDE ide, User creator, String description, String filePath, String originPath,
                 AddonStatus status, int totalVotes, int score, int downloads, LocalDateTime createDate, Set<Tag> tags) {
        this.id = id;
        this.name = name;
        this.ide = ide;
        this.creator = creator;
        this.description = description;
        this.filePath = filePath;
        this.originPath = originPath;
        this.status = status;
        this.totalVotes = totalVotes;
        this.score = score;
        this.downloads = downloads;
        this.createDate = createDate;
        this.tags = tags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IDE getIde() {
        return ide;
    }

    public void setIde(IDE ide) {
        this.ide = ide;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getOriginPath() {
        return originPath;
    }

    public void setOriginPath(String originPath) {
        this.originPath = originPath;
    }

    public AddonStatus getStatus() {
        return status;
    }

    public void setStatus(AddonStatus status) {
        this.status = status;
    }

    public int getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(int totalVotes) {
        this.totalVotes = totalVotes;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getDownloads() {
        return downloads;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<User> getRaters() {
        return raters;
    }

    public void setRaters(Set<User> raters) {
        this.raters = raters;
    }

    public double getRating() {
        if (getTotalVotes() == 0) {
            return 0;
        }
        return (double) getScore() / (double) getTotalVotes();
    }

    public String getLastCommitMessage() {
        return lastCommitMessage;
    }

    public void setLastCommitMessage(String lastCommitMessage) {
        this.lastCommitMessage = lastCommitMessage;
    }

    public LocalDateTime getLastCommitDate() {
        return lastCommitDate;
    }

    public void setLastCommitDate(LocalDateTime lastCommitDate) {
        this.lastCommitDate = lastCommitDate;
    }

    public int getOpenIssuesCount() {
        return openIssuesCount;
    }

    public void setOpenIssuesCount(int openIssuesCount) {
        this.openIssuesCount = openIssuesCount;
    }

    public int getPullRequestsCount() {
        return pullRequestsCount;
    }

    public void setPullRequestsCount(int pullRequestsCount) {
        this.pullRequestsCount = pullRequestsCount;
    }
}
