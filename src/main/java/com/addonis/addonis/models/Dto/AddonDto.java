package com.addonis.addonis.models.Dto;

import org.hibernate.validator.constraints.URL;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

public class AddonDto {

    @Size(min = 3, max = 30, message = "Name must be between 3 and 30 symbols.")
    private String name;

    @NotNull(message = "Must select a target IDE.")
    private int ideId;

    @NotBlank(message = "Description cannot be empty.")
    private String description;

    @NotNull(message = "Attached file cannot be empty.")
    private MultipartFile file;

    @NotBlank(message = "GitHub origin URL cannot be empty.")
    @URL(host = "github.com", message = "The URL must be from github.com")
    private String originPath;

    @NotEmpty(message = "Must select at least one tag.")
    private Set<Integer> tagIds;

    public AddonDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdeId() {
        return ideId;
    }

    public void setIdeId(int ideId) {
        this.ideId = ideId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getOriginPath() {
        return originPath;
    }

    public void setOriginPath(String originPath) {
        this.originPath = originPath;
    }

    public Set<Integer> getTagIds() {
        return tagIds;
    }

    public void setTagIds(Set<Integer> tagIds) {
        this.tagIds = tagIds;
    }
}
