package com.addonis.addonis.models.Dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDto {
    private static final int MIN_USERNAME_LENGTH = 2;
    private static final int MAX_USERNAME_LENGTH = 20;
    private static final int MIN_PASSWORD_LENGTH = 8;

    @Size(min = MIN_USERNAME_LENGTH, max = MAX_USERNAME_LENGTH,
            message = "Username must be between {min} and {max} characters long")
    private String username;

    @Size(min = MIN_PASSWORD_LENGTH, message = "Password should be at least {min} symbols")
    @Pattern(regexp = "^(?=.*[A-Z])(?=.*[0-9])(?=.*[-+_!@#$%^&*., ?]).+$", message = "Password should contains capital letter, digit and special symbol")
    private String password;

    @NotBlank(message = "Password can't be empty")
    private String confirmPassword;

    @NotBlank(message = "Email can't be empty")
    private String email;

    @Pattern(regexp = "[0-9]{10}", message = "Phone should contain 10 numbers")
    private String phone;

    public UserDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
