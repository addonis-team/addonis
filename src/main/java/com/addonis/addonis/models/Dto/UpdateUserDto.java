package com.addonis.addonis.models.Dto;

import javax.validation.constraints.Pattern;

public class UpdateUserDto {

    private String password;

    @Pattern(regexp = "^(?:(?=.{8,}$)(?=.*[A-Z])(?=.*[0-9])(?=.*[-+_!@#$%^&*., ?]).+)?$", message = "Password should contains capital letter, digit and special symbol and should be at least 8 symbols")
    private String newPassword;

    private String confirmPassword;

    private String email;

    @Pattern(regexp = "^(?:(?=.{10}$)[0-9]{10})?$", message = "Phone should contain 10 numbers")
    private String phone;

    public UpdateUserDto() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
