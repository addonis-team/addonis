package com.addonis.addonis.models.utils;

public class ModelConstants {

    public static final int MOST_POPULAR_ADDONS_DISPLAYED = 5;
    public static final int MOST_RECENT_ADDONS_DISPLAYED = 5;

    public static final int ADDON_STATUS_ACTIVE_ID = 1;
    public static final int ADDON_STATUS_PENDING_ID = 2;
    public static final int ADDON_STATUS_FEATURED_ID = 3;

    public static final int ADMIN_ROLE_ID = 1;
    public static final int USER_ROLE_ID = 2;

    public static final int USER_STATUS_DELETED_ID = 3;
    public static final int USER_STATUS_BLOCK_ID = 2;
    public static final int USER_STATUS_ACTIVE_ID = 1;
}
