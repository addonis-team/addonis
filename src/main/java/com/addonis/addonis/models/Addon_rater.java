package com.addonis.addonis.models;

import javax.persistence.*;

@Entity
@Table(name = "addon_rater")
public class Addon_rater {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "addon_id")
    private int addon_id;

    @Column(name = "user_id")
    private int user_id;

    @Column(name = "rating")
    private int rating;

    public Addon_rater() {
    }

    public Addon_rater(int addon_id, int user_id, int rating) {
        this.addon_id = addon_id;
        this.user_id = user_id;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAddon_id() {
        return addon_id;
    }

    public void setAddon_id(int addon_id) {
        this.addon_id = addon_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
