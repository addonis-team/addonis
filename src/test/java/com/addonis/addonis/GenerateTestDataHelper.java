package com.addonis.addonis;

import com.addonis.addonis.models.*;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.Set;

import static com.addonis.addonis.models.utils.ModelConstants.*;

public class GenerateTestDataHelper {

    public static Addon createMockAddon() {
        return new Addon(1, "Mock Addon", createMockIDE(), createMockUser(),
                "Mock addon description", "mock/addon/filepath", "mock/addon/originpath",
                createMockAddonStatus("active"), 10, 95, 100, LocalDateTime.now(), createMockAddonTags()
        );

    }

    public static MultipartFile createMockMultipartFile() {
        byte[] mockContent = {1, 2, 3, 4, 5};
        return new MockMultipartFile("mockFile", mockContent);
    }

    public static User createMockAdmin() {
        return new User(1, "mockAdmin", "mockPassword", "mockadmin@mock.com",
                "1234567890", createMockUserRole("admin"), createMockUserStatus("active"),
                "/mock/profile/picture/path");
    }

    public static User createMockUser() {
        return new User(2, "mockUser", "mockPassword", "mockuser@mock.com",
                "1234567890", createMockUserRole("user"), createMockUserStatus("active"),
                "/mock/profile/picture/path");
    }

    public static IDE createMockIDE() {
        return new IDE(1, "Mock IDE");
    }

    public static AddonStatus createMockAddonStatus(String status) {
        switch (status.toLowerCase()) {
            case "active":
                return new AddonStatus(ADDON_STATUS_ACTIVE_ID, "Active");
            case "pending":
                return new AddonStatus(ADDON_STATUS_PENDING_ID, "Pending");
            case "featured":
                return new AddonStatus(ADDON_STATUS_FEATURED_ID, "Featured");
        }
        throw new IllegalArgumentException("No such addon status.");
    }

    public static Set<Tag> createMockAddonTags() {
        return Set.of(new Tag(2, "Coding"), new Tag(4, "Services"));
    }

    public static UserRole createMockUserRole(String role) {
        switch (role.toLowerCase()) {
            case "admin":
                return new UserRole(ADMIN_ROLE_ID, "Admin");
            case "user":
                return new UserRole(USER_ROLE_ID, "User");
        }
        throw new IllegalArgumentException("No such role.");
    }

    public static UserStatus createMockUserStatus(String status) {
        switch (status.toLowerCase()) {
            case "active":
                return new UserStatus(USER_STATUS_ACTIVE_ID, "Active");
            case "blocked":
                return new UserStatus(USER_STATUS_BLOCK_ID, "Blocked");
            case "deleted":
                return new UserStatus(USER_STATUS_DELETED_ID, "Deleted");
        }
        throw new IllegalArgumentException("No such user status");
    }
}
