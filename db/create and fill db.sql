-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.4-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for addonis
DROP DATABASE IF EXISTS `addonis`;
CREATE DATABASE IF NOT EXISTS `addonis` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `addonis`;

-- Dumping structure for table addonis.addons
DROP TABLE IF EXISTS `addons`;
CREATE TABLE IF NOT EXISTS `addons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `IDE_id` int(11) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `description` tinytext DEFAULT NULL,
  `file_path` tinytext DEFAULT NULL,
  `origin_path` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `total_votes` int(11) NOT NULL DEFAULT 0,
  `score` int(11) NOT NULL DEFAULT 0,
  `downloads` int(11) NOT NULL DEFAULT 0,
  `create_date` datetime NOT NULL,
  `last_commit_message` varchar(100) DEFAULT NULL,
  `last_commit_date` datetime DEFAULT NULL,
  `open_issues_count` int(11) DEFAULT 0,
  `pull_requests_count` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `addons_name_uindex` (`name`),
  KEY `addons_ides_fk` (`IDE_id`),
  KEY `addons_status_fk` (`status`),
  KEY `addons_users_fk` (`creator_id`),
  KEY `addons_addon_files_fk` (`file_path`(255)),
  CONSTRAINT `addons_ides_fk` FOREIGN KEY (`IDE_id`) REFERENCES `ides` (`id`),
  CONSTRAINT `addons_status_fk` FOREIGN KEY (`status`) REFERENCES `addon_status` (`id`),
  CONSTRAINT `addons_users_fk` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Dumping data for table addonis.addons: ~14 rows (approximately)
/*!40000 ALTER TABLE `addons` DISABLE KEYS */;
INSERT INTO `addons` (`id`, `name`, `IDE_id`, `creator_id`, `description`, `file_path`, `origin_path`, `status`, `total_votes`, `score`, `downloads`, `create_date`, `last_commit_message`, `last_commit_date`, `open_issues_count`, `pull_requests_count`) VALUES
	(1, 'Best Addon', 1, 3, 'This is a demo addon with many downloads and a high score (9.6)', '\\1_Best Addon\\Best Addon.txt', 'http://github.com/hello', 3, 317335, 1523208, 715000, '2019-08-09 15:21:19', NULL, NULL, 0, 0),
	(2, 'MidTier Addon', 2, 5, 'This is a demo addon that has significant downloads and an okay rating (7.5)', '\\2_MidTier Addon\\MidTier Addon.txt', 'http://github.com/hello', 1, 63523, 238211, 125334, '2020-05-29 23:22:00', NULL, NULL, 0, 0),
	(3, 'Okay Addon', 2, 3, 'This is a demo addon that has some downloads and an average rating', '\\3_Okay Addon\\Okay Addon.txt', 'http://github.com/hello', 1, 4395, 11647, 37425, '2020-05-28 16:14:47', NULL, NULL, 0, 0),
	(4, 'Worst Addon', 4, 4, 'This is a demo addon that has few downloads and a low rating', '\\4_Worst Addon\\Worst Addon.txt', 'http://github.com/hello', 1, 1, 2, 3, '2020-07-06 10:20:39', NULL, NULL, 0, 0),
	(5, 'Runtime Terror', 1, 1, 'An addon named after the best buddy group in Telerik\'s Alpha Academy A-33 Java cohort', '\\5_Runtime Terror\\Runtime Terror.txt', 'http://github.com/hello', 1, 750000, 3750000, 1325716, '2019-10-19 18:21:53', NULL, NULL, 0, 0),
	(6, 'Spice Harvester', 3, 5, 'I must not fear. Fear is the mind-killer. Fear is the little-death that brings total obliteration. I will face my fear. I will permit it to pass over me and through me. Only I will remain.', '\\6_Spice Harvester\\Spice Harvester.txt', 'http://github.com/hello', 2, 0, 0, 0, '2021-12-15 16:16:26', NULL, NULL, 0, 0),
	(7, 'Pending Addon', 2, 9, 'This is a demo pending addon', '\\7_Pending Addon\\Pending Addon.txt', 'http://github.com/hello', 2, 0, 0, 0, '2021-12-15 16:16:50', NULL, NULL, 0, 0),
	(8, 'DeliverIT Scanner', 1, 3, 'Automate your parcel placement and listing needs today!', '\\8_DeliverIT Scanner\\DeliverIT Scanner.txt', 'http://github.com/hello', 1, 50233, 251165, 87533, '2021-11-17 19:22:15', NULL, NULL, 0, 0),
	(9, 'Bug Sniffer', 1, 6, 'This addon is meant to abuse your apps and break them in order to sniff out any bugs you may have overlooked', '\\9_Bug Sniffer\\Bug Sniffer.txt', 'http://github.com/hello', 3, 120000, 540000, 345007, '2016-11-20 09:23:42', NULL, NULL, 0, 0),
	(10, 'Java Support', 2, 6, 'Java Support for Microsoft Visual Studio', '\\10_Java Support\\Java Support.txt', 'http://github.com/hello', 3, 13233, 52932, 78332, '2005-02-08 02:17:29', NULL, NULL, 0, 0),
	(11, 'C# Support', 3, 6, 'C# Support for Eclipse IDE', '\\11_C# Support\\C# Support.txt', 'https://github.com/octocat/Hello-World', 1, 17325, 72765, 39917, '2011-08-22 19:21:08', 'Merge pull request #6 from Spaceghost/patch-1\n\nNew line at end of file.', '2012-03-06 23:06:50', 372, 622),
	(12, 'Git Control', 4, 2, 'Prevents from losing your uncommited changes when merging from Git source', '\\12_Git Control\\Git Control.txt', 'https://github.com/octocat/Hello-World', 3, 7523, 30844, 43233, '2010-06-19 07:21:33', 'Merge pull request #6 from Spaceghost/patch-1\n\nNew line at end of file.', '2012-03-06 23:06:50', 372, 622),
	(13, 'Addon Mate', 4, 4, 'Addon mate is an addon for addons. It manages your installed addons.', '\\13_AddonTest23\\nostalgia-landscape.7z', 'http://github.com/hello', 1, 33333, 156665, 99999, '2018-01-11 10:25:37', NULL, NULL, 0, 0),
	(15, 'Back in Business', 1, 3, 'Gets you back in the game quickly', '\\15_Back in Business\\Dummy addon file.txt', 'http://github.com/hello', 2, 0, 0, 0, '2021-12-11 17:24:17', NULL, NULL, 0, 0);
/*!40000 ALTER TABLE `addons` ENABLE KEYS */;

-- Dumping structure for table addonis.addons_tags
DROP TABLE IF EXISTS `addons_tags`;
CREATE TABLE IF NOT EXISTS `addons_tags` (
  `addon_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  KEY `addons_tags_addons_fk` (`addon_id`),
  KEY `addons_tags_tags_fk` (`tag_id`),
  CONSTRAINT `addons_tags_addons_fk` FOREIGN KEY (`addon_id`) REFERENCES `addons` (`id`),
  CONSTRAINT `addons_tags_tags_fk` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table addonis.addons_tags: ~24 rows (approximately)
/*!40000 ALTER TABLE `addons_tags` DISABLE KEYS */;
INSERT INTO `addons_tags` (`addon_id`, `tag_id`) VALUES
	(13, 4),
	(13, 2),
	(15, 9),
	(15, 4),
	(2, 9),
	(2, 4),
	(9, 2),
	(9, 9),
	(3, 6),
	(3, 5),
	(6, 7),
	(7, 2),
	(7, 9),
	(7, 10),
	(10, 3),
	(4, 11),
	(11, 3),
	(12, 9),
	(12, 5),
	(5, 7),
	(1, 9),
	(1, 2),
	(8, 10),
	(8, 9);
/*!40000 ALTER TABLE `addons_tags` ENABLE KEYS */;

-- Dumping structure for table addonis.addon_rater
DROP TABLE IF EXISTS `addon_rater`;
CREATE TABLE IF NOT EXISTS `addon_rater` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `addon_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `addon_rater_addons_fk` (`addon_id`),
  KEY `addon_rater_users_fk` (`user_id`),
  CONSTRAINT `addon_rater_addons_fk` FOREIGN KEY (`addon_id`) REFERENCES `addons` (`id`),
  CONSTRAINT `addon_rater_users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table addonis.addon_rater: ~8 rows (approximately)
/*!40000 ALTER TABLE `addon_rater` DISABLE KEYS */;
INSERT INTO `addon_rater` (`id`, `user_id`, `rating`, `addon_id`) VALUES
	(1, 1, 4, 1),
	(2, 2, 2, 1),
	(6, 5, 2, 1),
	(7, 5, 1, 8),
	(10, 2, 4, 6),
	(11, 2, 4, 5),
	(12, 2, 5, 12),
	(13, 1, 4, 7);
/*!40000 ALTER TABLE `addon_rater` ENABLE KEYS */;

-- Dumping structure for table addonis.addon_status
DROP TABLE IF EXISTS `addon_status`;
CREATE TABLE IF NOT EXISTS `addon_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `addon_status_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table addonis.addon_status: ~3 rows (approximately)
/*!40000 ALTER TABLE `addon_status` DISABLE KEYS */;
INSERT INTO `addon_status` (`id`, `name`) VALUES
	(1, 'Active'),
	(3, 'Featured'),
	(2, 'Pending');
/*!40000 ALTER TABLE `addon_status` ENABLE KEYS */;

-- Dumping structure for table addonis.followed
DROP TABLE IF EXISTS `followed`;
CREATE TABLE IF NOT EXISTS `followed` (
  `user_id` int(11) NOT NULL,
  `followed_id` int(11) NOT NULL,
  KEY `followed_users_followed_fk` (`followed_id`),
  KEY `followed_users_user_fk` (`user_id`),
  CONSTRAINT `followed_users_followed_fk` FOREIGN KEY (`followed_id`) REFERENCES `users` (`id`),
  CONSTRAINT `followed_users_user_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table addonis.followed: ~19 rows (approximately)
/*!40000 ALTER TABLE `followed` DISABLE KEYS */;
INSERT INTO `followed` (`user_id`, `followed_id`) VALUES
	(7, 1),
	(7, 2),
	(7, 3),
	(7, 4),
	(7, 5),
	(7, 6),
	(7, 8),
	(3, 1),
	(3, 2),
	(5, 1),
	(5, 2),
	(5, 3),
	(1, 5),
	(1, 7),
	(1, 6),
	(2, 3),
	(2, 5),
	(2, 6),
	(2, 7);
/*!40000 ALTER TABLE `followed` ENABLE KEYS */;

-- Dumping structure for table addonis.ides
DROP TABLE IF EXISTS `ides`;
CREATE TABLE IF NOT EXISTS `ides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDEs_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table addonis.ides: ~5 rows (approximately)
/*!40000 ALTER TABLE `ides` DISABLE KEYS */;
INSERT INTO `ides` (`id`, `name`) VALUES
	(4, 'Android Studio'),
	(3, 'Eclipse'),
	(1, 'IntelliJ'),
	(2, 'Microsoft Visual Studio'),
	(5, 'pyCharm');
/*!40000 ALTER TABLE `ides` ENABLE KEYS */;

-- Dumping structure for table addonis.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table addonis.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`) VALUES
	(1, 'Admin'),
	(2, 'User');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table addonis.tags
DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table addonis.tags: ~10 rows (approximately)
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` (`id`, `name`) VALUES
	(2, 'Coding'),
	(11, 'Database'),
	(3, 'Languages'),
	(4, 'Services'),
	(5, 'Source Control'),
	(6, 'Team Development'),
	(7, 'Themes'),
	(9, 'Tools'),
	(8, 'Tutorials'),
	(10, 'Web');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Dumping structure for table addonis.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `profile_picture_path` tinytext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_uindex` (`email`),
  UNIQUE KEY `users_phone_uindex` (`phone`),
  UNIQUE KEY `users_username_uindex` (`username`),
  KEY `users_roles_fk` (`role_id`),
  KEY `users_user_status_fk` (`status_id`),
  KEY `users_profile_pictures_fk` (`profile_picture_path`(255)),
  CONSTRAINT `users_roles_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `users_user_status_fk` FOREIGN KEY (`status_id`) REFERENCES `user_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table addonis.users: ~10 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `email`, `phone`, `role_id`, `status_id`, `profile_picture_path`) VALUES
	(1, 'angel', 'angel', 'angel@mail.com', '0887112233', 1, 1, '\\user_1\\1.jpg'),
	(2, 'kristina', 'kristina', 'kristina@mail.com', '0886891289', 1, 1, '<null>'),
	(3, 'demouser', 'demouser', 'demouser@mail.com', '0998332443', 2, 1, '\\user_3\\3.jpg'),
	(4, 'blockeduser', 'blockeduser', 'blockeduser@mail.com', '0778332443', 2, 2, NULL),
	(5, 'zeus', 'zeus', 'zeus@mail.com', '0665113224', 2, 1, '\\user_5\\4.jpg'),
	(6, 'krypt0', 'krypt0', 'krypt0@mail.com', '0332997885', 2, 1, NULL),
	(7, 'jester', 'jester', 'jester@mail.com', '0111222333', 2, 1, NULL),
	(8, 'justblocked', 'justblocked', 'justblocked@mail.com', '1122334455', 2, 2, NULL),
	(9, 'peny', 'peny', 'peny@mail.com', '0888888899', 2, 1, '\\user_9\\2.jpg'),
	(13, 'deleteduser', 'deleted', 'deleted@mail.com', '0999111222', 2, 3, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table addonis.user_status
DROP TABLE IF EXISTS `user_status`;
CREATE TABLE IF NOT EXISTS `user_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_status_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table addonis.user_status: ~3 rows (approximately)
/*!40000 ALTER TABLE `user_status` DISABLE KEYS */;
INSERT INTO `user_status` (`id`, `name`) VALUES
	(1, 'Active'),
	(2, 'Blocked'),
	(3, 'Deleted');
/*!40000 ALTER TABLE `user_status` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
